Pod::Spec.new do |s|
  s.name         = "FWGIFImageView"
  s.version      = "0.0.1"
  s.summary      = "Play GIF image use UIImageView low memory."
  s.homepage     = "https://bitbucket.org/CyonLeu/fwgifimageview/src"
  # s.license      = "MIT (example)"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "CyonLeu" => "cyonleu@126.com" }
  s.social_media_url   = "http://twitter.com/CyonLeu"
  s.platform     = :ios
  r.source       = { :git => "https://CyonLeu@bitbucket.org/CyonLeu/fwgifimageview.git", :tag => "0.0.1" }
  s.source_files  = "FWGIFImageView", "FWGIFImageView/*.{h,m}"
  #s.exclude_files = "Classes/Exclude"

  s.frameworks = "Foundatior", "ImageIO""

  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
